FROM rocker/rstudio:4.0.0

LABEL license="GPL-2.0"

RUN apt-get update && apt-get install -y \
    apt-get install zlib1g-dev
